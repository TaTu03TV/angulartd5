import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Passager, IPassagerDto } from '../models/passager.model';

@Injectable({
  providedIn: 'root'
})
export class PassagerService {
  private readonly urlBase = 'https://randomuser.me/api?';

  constructor(private http: HttpClient) {}

  getPassagers(icao: string): Observable<Passager[]> {
    return this.http.get<{ results: IPassagerDto[] }>(`${this.urlBase}results=20&inc=name,picture,email&seed=${icao}`)
      .pipe(map(response => response.results.map(dto => new Passager(dto))));
  }
}