import { Component,Input } from '@angular/core';
import { IVol } from '../../models/vol.model';
import { CommonModule } from '@angular/common';
import {MatIconModule} from '@angular/material/icon'


@Component({
  selector: 'app-vol',
  standalone: true,
  imports: [CommonModule, MatIconModule],
  templateUrl: './vol.component.html',
  styleUrls: ['./vol.component.scss']
})
export class VolComponent {

  @Input() vol!: IVol;
  @Input() type!: string;

  constructor() {}

  ngOnChanges() {
    console.log('VolComponent - ngOnChanges()');
    console.log(this.vol);
  }
}
