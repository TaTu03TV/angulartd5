import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PassagerComponent} from '../passager/passager.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule } from '@angular/forms';



@Component({
  selector: 'app-liste-passagers',
  standalone: true,
  imports: [CommonModule, PassagerComponent, MatSlideToggleModule, FormsModule],
  templateUrl: './liste-passagers.component.html',
  styleUrls: ['./liste-passagers.component.scss']
})
export class ListePassagersComponent {
  @Input() passagers: any[] = [];
  showPhotos = false;
}
