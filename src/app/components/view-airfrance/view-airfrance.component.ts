import { Component, OnInit } from '@angular/core';
import { FiltresComponent } from '../filtres/filtres.component';
import { ListeVolsComponent } from '../liste-vols/liste-vols.component';
import { ListePassagersComponent } from '../liste-passagers/liste-passagers.component';
import { IAeroport } from '../../models/aeroport.model';
import { VolService } from '../../services/vol.service';
import { Vol } from '../../models/vol.model';
import { Passager } from '../../models/passager.model';
import { PassagerService } from '../../services/passager.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-view-airfrance',
  standalone: true,
  imports: [FiltresComponent, ListeVolsComponent, ListePassagersComponent],
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent implements OnInit {
  type!: string;
  listeVols: any[] = [];
  volSelectionne!: Vol;
  passagers: Passager[] = [];

  constructor(private route: ActivatedRoute, private volService: VolService, private passagerService: PassagerService) {}

  appliquerFiltres(filtres: { aeroport: IAeroport, dateDebut: Date, dateFin: Date }) {
    const debut = filtres.dateDebut.getTime() / 1000;
    const fin = filtres.dateFin.getTime() / 1000;

    if (this.type === 'decollages') {
      this.volService.getVolsDepart(filtres.aeroport.icao, debut, fin).subscribe(vols => {
        this.listeVols = vols;
      });
    } else if (this.type === 'atterrissages') {
      this.volService.getVolsArrivee(filtres.aeroport.icao, debut, fin).subscribe(vols => {
        this.listeVols = vols;
      });
    }
  }

  getPassagers(vol: Vol) {
    this.passagerService.getPassagers(vol.icao).subscribe(passagers => {
      this.passagers = passagers;
    });
  }

  gererVolSelectionne(vol: Vol) {
    this.volSelectionne = vol;
    this.getPassagers(vol);
  }

  ngOnChanges() {
    console.log(this.volSelectionne);
  }

  ngOnInit() {
    this.route.url.subscribe(url => {
      this.type = url[0].path;
    });
  }
}
