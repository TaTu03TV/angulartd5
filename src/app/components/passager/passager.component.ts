import { Component, Input } from '@angular/core';
import {MatIconModule} from '@angular/material/icon'
import { ClasseVolDirective } from '../../directives/classeVol.directive';
import  {BagagesDirective} from '../../directives/bagages.directive';
import { CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material/tooltip';




@Component({
  selector: 'app-passager',
  standalone: true,
  imports: [MatIconModule, ClasseVolDirective, BagagesDirective, CommonModule, MatTooltipModule],
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.scss']
})
export class PassagerComponent {
  @Input() passager: any;
  @Input() showPhotos = false;

}
