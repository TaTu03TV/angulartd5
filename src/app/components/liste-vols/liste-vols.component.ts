import { Component, Input, Output, EventEmitter } from '@angular/core';
import { VolComponent } from '../vol/vol.component';
import { Vol } from '../../models/vol.model';

@Component({
  selector: 'app-liste-vols',
  standalone: true,
  imports: [VolComponent],
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent {
  @Input() listeVols!: any[];
  @Input() type!: string;

  @Output() volSelectionne = new EventEmitter<Vol>();


  selectionnerVol(vol: Vol) {
    this.volSelectionne.emit(vol);
  }
}