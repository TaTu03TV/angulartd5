import { Directive, ElementRef, Input, OnChanges } from '@angular/core';

@Directive({
  selector: '[appClasseVol]',
  standalone: true
})
export class ClasseVolDirective implements OnChanges {
  @Input('appClasseVol') classeVol!: string;

  constructor(private el: ElementRef) {}

  ngOnChanges() {
    switch (this.classeVol) {
      case 'BUSINESS':
        this.el.nativeElement.style.color = 'red';
        break;
      case 'PREMIUM':
        this.el.nativeElement.style.color = 'green';
        break;
      case 'STANDARD':
        this.el.nativeElement.style.color = 'blue';
        break;
      default:
        this.el.nativeElement.style.color = 'black';
        break;
    }
  }
}