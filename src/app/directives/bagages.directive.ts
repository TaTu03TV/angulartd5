import { Directive, ElementRef, Input, OnChanges } from '@angular/core';

@Directive({
  selector: '[appBagages]',
  standalone: true
})
export class BagagesDirective implements OnChanges {
  @Input('appBagages') bagages!: { classeVol: string, nbBagagesSoute: number };

  constructor(private el: ElementRef) {}

  ngOnChanges() {
    let maxBagages: number;
    switch (this.bagages.classeVol) {
      case 'STANDARD':
        maxBagages = 1;
        break;
      case 'BUSINESS':
        maxBagages = 2;
        break;
      case 'PREMIUM':
        maxBagages = 3;
        break;
      default:
        maxBagages = 0;
        break;
    }

    if (this.bagages.nbBagagesSoute > maxBagages) {
      this.el.nativeElement.style.backgroundColor = 'red';
    } else {
      this.el.nativeElement.style.backgroundColor = '';
    }
  }
}